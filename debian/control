Source: beansbinding
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Marek Slama <marek.slama@sun.com>,
 tony mancill <tmancill@debian.org>
Build-Depends: debhelper-compat (= 13),
 javahelper, ant, default-jdk,
 maven-repo-helper
Standards-Version: 4.6.2
Homepage: https://github.com/JFormDesigner/swing-beansbinding
Vcs-Browser: https://salsa.debian.org/java-team/beansbinding
Vcs-Git: https://salsa.debian.org/java-team/beansbinding.git
Rules-Requires-Root: no

Package: libbeansbinding-java
Architecture: all
Depends: ${java:Depends}, ${misc:Depends}
Suggests: libbeansbinding-java-doc
Multi-Arch: foreign
Description: Beans Binding API (library)
 In essence, Beans Binding (JSR 295) is about keeping
 two properties (typically of two objects) in sync.
 An additional emphasis is placed on the ability
 to bind to Swing components, and easy integration
 with IDEs such as NetBeans. This project provides
 the reference implementation.
 .
 This package contains the Java Beans Binding library.

Package: libbeansbinding-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: ${java:Recommends}
Multi-Arch: foreign
Description: Beans Binding API (documentation)
 In essence, Beans Binding (JSR 295) is about keeping
 two properties (typically of two objects) in sync.
 An additional emphasis is placed on the ability
 to bind to Swing components, and easy integration
 with IDEs such as NetBeans. This project provides
 the reference implementation.
 .
 This package contains Javadoc API documentation.
