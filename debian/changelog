beansbinding (1.2.1-5) unstable; urgency=medium

  [ Vladimir Petko ]
  * d/rules, d/p/*: pass Java source and target level on Ant command
    line, drop Java compatibility patch (Closes: #1057491).

  [ Debian Janitor ]
  * Add Multi-Arch: foreign hint to libbeansbinding-java and
    libbeansbinding-java-doc
  * Use debhelper 13.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ tony mancill ]
  * Bump Standards-Version to 4.6.2
  * Set Rules-Requires-Root: no in debian/control

 -- tony mancill <tmancill@debian.org>  Sun, 28 Jan 2024 21:18:42 -0800

beansbinding (1.2.1-4) unstable; urgency=medium

  * Team upload.
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.1.3.
  * Remove trailing whitespace
  * Fix FTBFS with Java 9. (Closes: #893100)
  * Update changelog
  * Remove libbeansbinding-java-doc.doc-base because jh_javadoc provided the
    same information automatically. This change prevents adding doc-base files
    twice. Thanks to Laurent Bonnaud for the report. (Closes: #889655)

 -- Markus Koschany <apo@debian.org>  Fri, 23 Mar 2018 15:04:13 +0100

beansbinding (1.2.1-3) unstable; urgency=medium

  * Correct Vcs URLs (Closes: #873445)
  * Re-import the upstream orig.tar.gz with gbp import-orig so it matches
    the tarball in the archive.
  * Remove Andres Mejia <amejia@debian.org> from Uploaders (Closes: #743516)
  * Update Homepage with a reasonable placeholder (Closes: #738808)
    There isn't a definitive upstream homepage for this project that I
    could fine. The JFormsDesigner repo on Github at least preserves the
    upstream history.

 -- tony mancill <tmancill@debian.org>  Sun, 27 Aug 2017 14:33:14 -0700

beansbinding (1.2.1-2) unstable; urgency=medium

  [ Andres Mejia ]
  * Update to my @debian.org email.
  * Fix VCS entries.

  [ Andrew Ross ]
  * Install maven pom.

  [ tony mancill ]
  * Tweak maven metadata and build-dep on maven-repo-helper
  * Bump Standards-Version to 3.9.8.
  * Use debhelper 10.
  * Update Vcs URLs and Homepage.
  * Correct doc-base to be Programming/Java.
  * Add myself to Uploaders.

 -- tony mancill <tmancill@debian.org>  Sun, 26 Mar 2017 10:44:07 -0700

beansbinding (1.2.1-1) unstable; urgency=low

  [ Andres Mejia ]
  * Upload for Debian. (Closes: #492255)
  * Add Debian Java team as Maintainer, Marek and myself as Uploaders.
  * Create get-orig-source script to generate repacked upstream tarball.
  * Use 3.0 (quilt) source format.
  * Use debhelper 7 format.
  * Add watch file.
  * Bump to Standards-Version 3.9.1.
  * Add Vcs entries to control file.
  * Update packaging to use dh with javahelper.
  * Rename source package to use upstream name, beansbinding.

  [ Damien Raude-Morvan ]
  * d/control: Drop Depends: default-jre | java2-runtime
    as a Java library don't need to depends on runtime (Java Policy).

 -- Andres Mejia <mcitadel@gmail.com>  Fri, 21 Jan 2011 22:38:29 -0500

libbeansbinding-java (1.2.1-0ubuntu3) intrepid; urgency=low

  * replace explicit dependency on openjdk-6-jre (LP: #259393)
  * remove redundant string from .dirs file
  * remove log file

 -- Yulia Novozhilova <Yulia.Novozhilova@sun.com>  Tue, 19 Aug 2008 19:08:49 +0400

libbeansbinding-java (1.2.1-0ubuntu2) hardy; urgency=low

  * Replace icedtea-java7 dependencies with openjdk-6 (LP: #203636)

 -- Marek Slama <marek.slama@sun.com>  Thu, 20 Mar 2008 10:00:00 +0100

libbeansbinding-java (1.2.1-0ubuntu1) hardy; urgency=low

  * Initial version. (LP: #185975)

 -- Marek Slama <marek.slama@sun.com>  Fri, 25 Jan 2008 10:00:00 +0100
